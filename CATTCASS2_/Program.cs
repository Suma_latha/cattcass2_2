﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CATTCASS2_
{
    class Program
    {
        static void Main(string[] args)
        {

            //declaring a variable of type (reference type) string
            string stores_exam;

            //assigning value to the reference type variable
            stores_exam = "exam";

            //declaring a variable of type (value type) int
            int stores_number;

            //assigning value to the value type variable 
            stores_number = 483;

            //we need to disply word 'exam483' thus declaring another variable of reference type
            string final_word;

            //assigning sum of vlues of both value type and reference type. 
            final_word = stores_exam + stores_number;

            //showing the content 
            Console.WriteLine("Main - the value of variable is " + final_word);

            //this holds the console window from vanishing
            Console.ReadLine();


        }

    }
}

